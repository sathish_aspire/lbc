@extends('layouts.dashboard')
@section('page_heading','Dashboard')
@section('section')
            <!-- /.row -->
            <div class="col-sm-12">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-rupee fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">26</div>
                                    <div>Total Expense</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-rupee fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">12</div>
                                    <div>Month Wise Exp</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-shopping-cart fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">124</div>
                                    <div>Total LBC Fund</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-rupee fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">13</div>
                                    <div>Month LBC Fund</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-rupee fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">13</div>
                                    <div>Total Pending</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.row -->
<div class="col-sm-12">
<div class="row">
    <div class="col-sm-6">
    @section ('stable_panel_title','Current week Birthday Celebration')
        @section ('stable_panel_body')
        @include('widgets.table', array('class'=>'table-striped'))
        @endsection
        @include('widgets.panel', array('header'=>true, 'as'=>'stable'))
    </div>
    <div class="col-sm-6">
        @section ('htable_panel_title','Current week Anniversary Celebration')
        @section ('htable_panel_body')
        @include('widgets.table', array('class'=>'table-hover'))
        @endsection
        @include('widgets.panel', array('header'=>true, 'as'=>'htable'))
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        @section ('cotable_panel_title','Current month Birthday Celebration')
        @section ('cotable_panel_body')
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Address</th>
                </tr>
            </thead>
            <tbody>
                <tr class="success">
                    <td>John</td>
                    <td>john@gmail.com</td>
                    <td>London, UK</td>
                </tr>
                <tr>
                    <td>Wayne</td>
                    <td>wayne@gmail.com</td>
                    <td>Manchester, UK</td>
                </tr>
                <tr class="info">
                    <td>Andy</td>
                    <td>andy@gmail.com</td>
                    <td>Merseyside, UK</td>
                </tr>
                <tr>
                    <td>Danny</td>
                    <td>danny@gmail.com</td>
                    <td>Middlesborough, UK</td>
                </tr>
                <tr class="warning">
                    <td>Frank</td>
                    <td>frank@gmail.com</td>
                    <td>Southampton, UK</td>
                </tr>
                <tr>
                    <td>Scott</td>
                    <td>scott@gmail.com</td>
                    <td>Newcastle, UK</td>
                </tr>
                <tr class="danger">
                    <td>Rickie</td>
                    <td>rickie@gmail.com</td>
                    <td>Burnley, UK</td>
                </tr>
            </tbody>
        </table>    
        @endsection
        @include('widgets.panel', array('header'=>true, 'as'=>'cotable'))
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        @section ('cotable_panel_title','Current month Anniversary Celebration')
        @section ('cotable_panel_body')
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Address</th>
                </tr>
            </thead>
            <tbody>
                <tr class="success">
                    <td>John</td>
                    <td>john@gmail.com</td>
                    <td>London, UK</td>
                </tr>
                <tr>
                    <td>Wayne</td>
                    <td>wayne@gmail.com</td>
                    <td>Manchester, UK</td>
                </tr>
                <tr class="info">
                    <td>Andy</td>
                    <td>andy@gmail.com</td>
                    <td>Merseyside, UK</td>
                </tr>
                <tr>
                    <td>Danny</td>
                    <td>danny@gmail.com</td>
                    <td>Middlesborough, UK</td>
                </tr>
                <tr class="warning">
                    <td>Frank</td>
                    <td>frank@gmail.com</td>
                    <td>Southampton, UK</td>
                </tr>
                <tr>
                    <td>Scott</td>
                    <td>scott@gmail.com</td>
                    <td>Newcastle, UK</td>
                </tr>
                <tr class="danger">
                    <td>Rickie</td>
                    <td>rickie@gmail.com</td>
                    <td>Burnley, UK</td>
                </tr>
            </tbody>
        </table>    
        @endsection
        @include('widgets.panel', array('header'=>true, 'as'=>'cotable'))
    </div>
</div>
</div>
             <!-- /.col-lg-4 -->            
@stop
