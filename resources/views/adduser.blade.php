@extends ('layouts.dashboard')
@section('section')
<div class="col-sm-12">
    <div class="row">
        <h1>Add User</H1>
    </diV>
    <div class="row">
        <div class="col-lg-6">
            <form role="form">
                <div class="form-group">
                    <label>Name</label>
                    <input class="form-control" placeholder="Enter name">
                </div>
                <div class="form-group">
                    <label>ACE No</label>
                    <input class="form-control" placeholder="Enter ace no">
                </div>
                <div class="form-group">
                    <label>Email id</label>
                    <input class="form-control" placeholder="Enter email id">
                </div>
                <div class="form-group">
                    <label>Date Of Birth</label>
                    <input class="form-control" id="datepicker" placeholder="Enter date of birth">
                </div>
                <div class="form-group">
                    <label>Date Of Joining</label>
                    <input class="form-control" id="datepicker" placeholder="Enter date of joining">
                </div>
                <button type="submit" class="btn btn-default">Submit Button</button>
                <button type="reset" class="btn btn-default">Reset Button</button>
            </form>
        </div>
    </div>
</div>
@stop
