@extends ('layouts.dashboard')
@section('page_heading','Form')

@section('section')

<div class="row">
<div class="col-sm-3">
</div>
<div class="col-sm-6">
                <button  type="button" class="btn btn-primary" onclick="window.location='{{ url("adduser") }}'">Add User</button> 


</div>
<div class="col-sm-3">
</div>
</div>





<div class="col-sm-12">
<div class="row">
    <div class="col-sm-12">
        @section ('cotable_panel_title','List Of Users')
        @section ('cotable_panel_body')
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Remove User</th>
                    <th>Edit User</th>
                    <th>Add LBC</th>
                    <th>Update Role</th>



                </tr>
            </thead>
            <tbody>
                <tr class="success">
                    <td>John</td>
                    <td>john@gmail.com</td>
                    <td>London, UK</td>
                    <td><button type="button" class="btn btn-primary     ">Remove </button></td>
                    <td><button type="button" class="btn btn-primary     ">Edit </button></td>
                    <td><button type="button" class="btn btn-primary     ">Add LBC </button></td>
                    <td><button type="button" class="btn btn-primary     ">Update Role </button></td>                                        
                </tr>
                <tr>
                    <td>Wayne</td>
                    <td>wayne@gmail.com</td>
                    <td>London, UK</td>
                    <td><button type="button" class="btn btn-primary     ">Remove </button></td>
                    <td><button type="button" class="btn btn-primary     ">Edit </button></td>
                    <td><button type="button" class="btn btn-primary     ">Add LBC </button></td> 
                    <td><button type="button" class="btn btn-primary     ">Update Role </button></td>                                                           
                </tr>
                <tr class="info">
                    <td>Andy</td>
                    <td>andy@gmail.com</td>
                    <td>Merseyside, UK</td>
                    <td><button type="button" class="btn btn-primary     ">Remove </button></td>
                    <td><button type="button" class="btn btn-primary     ">Edit </button></td>
                    <td><button type="button" class="btn btn-primary     ">Add LBC </button></td>
                    <td><button type="button" class="btn btn-primary     ">Update Role </button></td>                                                            
                </tr>
                <tr>
                    <td>Danny</td>
                    <td>danny@gmail.com</td>
                    <td>Middlesborough, UK</td>
                    <td><button type="button" class="btn btn-primary     ">Remove </button></td>
                    <td><button type="button" class="btn btn-primary     ">Edit </button></td>
                    <td><button type="button" class="btn btn-primary     ">Add LBC </button></td>
                    <td><button type="button" class="btn btn-primary     ">Update Role </button></td>                                                             
                </tr>
                <tr class="warning">
                    <td>Frank</td>
                    <td>frank@gmail.com</td>
                    <td>Southampton, UK</td>
                    <td><button type="button" class="btn btn-primary     ">Remove </button></td>
                    <td><button type="button" class="btn btn-primary     ">Edit </button></td>
                    <td><button type="button" class="btn btn-primary     ">Add LBC </button></td>
                    <td><button type="button" class="btn btn-primary     ">Update Role </button></td>                                                             
                </tr>
                <tr>
                    <td>Scott</td>
                    <td>scott@gmail.com</td>
                    <td>Newcastle, UK</td>
                    <td><button type="button" class="btn btn-primary     ">Remove </button></td>
                    <td><button type="button" class="btn btn-primary     ">Edit </button></td>
                    <td><button type="button" class="btn btn-primary     ">Add LBC </button></td>
                    <td><button type="button" class="btn btn-primary     ">Update Role </button></td>                                                            
                </tr>
                <tr class="danger">
                    <td>Rickie</td>
                    <td>rickie@gmail.com</td>
                    <td>Burnley, UK</td>
                    <td><button type="button" class="btn btn-primary     ">Remove </button></td>
                    <td><button type="button" class="btn btn-primary     ">Edit </button></td>

                    <td><button type="button" class="btn btn-primary     ">Add LBC </button></td>
                    <td><button type="button" class="btn btn-primary     ">Update Role </button></td>                                                             
                </tr>
            </tbody>
        </table>    
        @endsection
        @include('widgets.panel', array('header'=>true, 'as'=>'cotable'))
    </div>
</div>

@stop