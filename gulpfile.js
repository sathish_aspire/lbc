var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
elixir(function(mix) {
	mix.styles([ 'jquery-ui.css',
	             'styles.css'], 'public/css/library.css');
	
	mix.scripts([ 'jquery-ui.js',
				  'frontend.js',
				  'jquery-1.12.4.js' ], 'public/js/library.js');
});