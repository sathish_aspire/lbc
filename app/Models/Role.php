<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public static $rules = array(
   		'name' => 'required'        
    );
    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'user_role', 
       'role_id', 'user_id');
    }
}