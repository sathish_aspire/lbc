<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','email','date_of_birth','date_of_joining', '                    total_amount'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public static $rules = array(
   		'name' => 'required',
        'email' => 'required',
        'date_of_birth' => 'required',
        'date_of_joining' => 'required',
        'total_amount' => 'required'
        
   );  
    public function expenses() 
    {
   		return $this->hasMany('App\Models\Expense', 'id');
    }
    public function payments() 
    {
        return $this->hasMany('App\Models\User_Payment', 'id');
    }
    public function roles()
    {
        return $this->belongsToMany('App\Models\Role', 'user_role', 
      'user_id', 'role_id');
    }
}
