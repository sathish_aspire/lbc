<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','description','expensed_amount','status'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    public static $rules = array(
   		'description' => 'required',
        'expensed_amount' => 'required',
        'status' => 'required'
    );  
   public function users() 
   {
   		return $this->belongsTo('App\Models\User', 'id');
   }
}



