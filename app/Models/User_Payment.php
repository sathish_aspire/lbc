<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Payment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','contributed_amount','mode_of_payment',
    						'references','date_of_payment'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public static $rules = array(
    	'contributed_amount' => 'required',
        'mode_of_payment' => 'required',
        'references' => 'required',
        'date_of_payment' => 'required'
    );  
    public function users() 
    {
   		return $this->belongsTo('App\Models\User', 'id');
    }
}

